
// Creating Security Group for EC2
resource "aws_security_group" "instance" {
  vpc_id      = var.vpc_id
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  
    }
  tags = {Name = "test"}
}
  

// Creating Launch Configuration
resource "aws_launch_configuration" "launch_config" {
  name                   = "launch_config"
  image_id               = data.aws_ami.amazon_linux_ecs.id
  instance_type          = "t3.small"
  security_groups        = [aws_security_group.instance.id]
  key_name               = var.key_name
  iam_instance_profile   = var.iam_instance_profile
  user_data = <<-EOF
              #!/bin/bash
              echo ECS_CLUSTER=ecs_teste01 >> /etc/ecs/ecs.config
              export ALB_INTERNET_NAME=var.aws_alb_name
              EOF
  lifecycle {
    create_before_destroy = true
  }
}
// Creating AutoScaling Group
resource "aws_autoscaling_group" "autoscaling_group" {
  launch_configuration          = aws_launch_configuration.launch_config.name
  min_size                      = var.values_min
  max_size                      = var.values_max
  target_group_arns             = [var.aws_lb_target_group.id]
  vpc_zone_identifier           = var.private_subnets
  health_check_type             = "ELB"
  tag {
    key = "name"
    value = "autoscaling-test"
    propagate_at_launch = true
  }
}

#For now we only use the AWS ECS optimized ami <https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html>
data "aws_ami" "amazon_linux_ecs" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-*-amazon-ecs-optimized"]
  }

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}