variable "key_name" {

}
variable "amis" {

}

variable "values_min" {
}

variable "values_max" {
}

variable "aws_region" {
}

variable "aws_alb" {

}

variable "vpc_id" {

}

variable "private_subnets" {

}

variable "azs" {
}

variable "tags" {
  default     = {}
}

variable "name" {
}

variable "aws_lb_target_group" {
  
}
variable "ecs_cluster" {
  
}
variable "iam_instance_profile" {
  
}

variable "aws_alb_name" {
  
}


