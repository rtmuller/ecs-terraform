data "aws_ecs_task_definition" "nginx" {
  task_definition = aws_ecs_task_definition.nginx.arn
}

resource "aws_ecs_task_definition" "nginx" {
    family                = "nginx"
    container_definitions = <<DEFINITION
[
  {
    "name": "nginx",
    "image": "nginx",
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80
      }
    ],
    "memory": 500,
    "cpu": 10
    } 
]
DEFINITION
}

resource "aws_ecs_service" "nginx" {
  	name            = "nginx"
  	iam_role        = aws_iam_role.ecs-service-role.name
  	cluster         = aws_ecs_cluster.test-ecs-cluster.id
  	task_definition = aws_ecs_task_definition.nginx.arn
    #task_definition = aws_ecs_task_definition.wordpress.family}:${max("${aws_ecs_task_definition.wordpress.revision}", "${data.aws_ecs_task_definition.wordpress.revision}")}"

  	desired_count   = 1

  	load_balancer {
    	target_group_arn  = var.aws_lb_target_group.arn
    	container_port    = 80
    	container_name    = "nginx"
	}
}